(function() {
    'use strict';
    /* fix holderjs */
    angular
        .module('VideoCatalog')
        .directive('vcdFixHolderjs', FixHolderjs); 
    
    function FixHolderjs() {
        return {
            link: function (scope, element, attrs) {
                Holder.run({ images: element[0], nocss: true });
            }
        };
    }
})();