(function() {
    'use strict';
    
    angular
        .module('VideoCatalog')
        .config(AppConfig);
        
    AppConfig.$inject = ['localStorageServiceProvider', '$locationProvider', '$tooltipProvider'];
    
    function AppConfig(localStorageServiceProvider, $locationProvider, $tooltipProvider) {
        localStorageServiceProvider.setPrefix('VideoCatalog');
        $tooltipProvider.setTriggers({
            'mouseenter': 'mouseleave',
            'click': 'click',
            'focus': 'blur',
            'never': 'mouseleave'
        });
        
        // $locationProvider.html5Mode(true); // check that article: https://github.com/angular-ui/ui-router/wiki
    }
})();