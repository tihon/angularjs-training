(function() {
    'use strict';
    
    angular
        .module('VideoCatalog')
        .run(MockHttp);
        
    MockHttp.$inject = ['$httpBackend', '$http', 'CoursesLocalStorageFactory'];        
        
    function MockHttp($httpBackend, $http, CoursesLocalStorageFactory) {
        
        $httpBackend.whenGET('app-storage/users').respond(function(method, url, data) {
                var users = [
                                {
                                    "id" : "1",
                                    "username" : "test",
                                    "password" : "test",
                                    "email" : "test@test.com"
                                },
                            
                                {
                                    "id" : "2",
                                    "username" : "tihon",
                                    "password" : "tihon123",
                                    "email" : "tihon.tihon@gmail.com"
                                }
                            ];
                return [200, users, {}];
            });            
         
        $httpBackend.whenGET('app-storage/authors').respond(function(method, url, data) {
                var authors = [
                                {
                                    "id" : 1,
                                    "name": "Papa Jhon"
                                },
                                {
                                    "id" : 2,
                                    "name" : "Jon Skeet"
                                },
                                {
                                    "id" : 3,
                                    "name" : "Anthony Lavey"
                                },
                                {
                                    "id" : 4,
                                    "name" : "Bill Gates"
                                }
                            ];
                            
                return [200, authors, {}];                            
            });          
         
        $httpBackend.whenGET('courses/list').respond(function(method, url, data) {
                
                var courses = CoursesLocalStorageFactory.query();
                
                if (courses.length === 0){
                        courses = [
                                        {
                                            "id" : "1",
                                            "name": "Video Course 1", 
                                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the", 
                                            "duration": "120",
                                            "startDate": "1439839032091",
                                            "authors": [ {"id":2,"name":"Jon Skeet"}, {"id":3,"name":"Anthony Lavey"} ]
                                        },
                                        
                                        {
                                            "id" : "2",
                                            "name": "Video Course 2", 
                                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the", 
                                            "duration": "220",
                                            "startDate": "1439839062091",
                                            "authors": [ {"id":2,"name":"Jon Skeet"} ]
                                        },
                                    
                                        {
                                            "id" : "3",
                                            "name": "Video Course 3", 
                                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the", 
                                            "duration": "45",
                                            "startDate": "1439831062091",
                                            "authors": [ {"id":3,"name":"Anthony Lavey"} ]
                                        }
                                    ];
                                    
                                    CoursesLocalStorageFactory.restoreLocalStorage(courses);      
                }          
                
                return [200, courses, {}];
        });     
         
        $httpBackend.whenGET(/courses\/\d+/i).respond(function(method, url, data) {
                var courseId = url.split('/')[1];
                var course = CoursesLocalStorageFactory.get(courseId);
                
                return [200, course, {}];
        });

        $httpBackend.whenDELETE(/courses\/\d+/).respond(function(method, url, data) {
            var courseId = url.split('/')[1];
            CoursesLocalStorageFactory.remove(courseId);
            
            return [204, {}, {}];
        });            
         
        $httpBackend.whenPOST('courses').respond(function(method, url, data) {
            var params = angular.fromJson(data);
            CoursesLocalStorageFactory.add(params);
            
            return [201, {}, {}];
        });         
         
        $httpBackend.whenPOST(/courses\/\d+/).respond(function(method, url, data) {
            
            var params = angular.fromJson(data);
            CoursesLocalStorageFactory.update(params);
            
            return [201, {}, {}];
        });         
         
        $httpBackend.whenGET(/.*[\.](html|json)$/i).passThrough();
    }
})();