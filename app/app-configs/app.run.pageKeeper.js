(function() {
    'use strict';
    
    angular
        .module('VideoCatalog')
        .run(PageKeeper);
            
    PageKeeper.$inject = ['$rootScope', '$state', '$cookieStore', '$http'];
    
    function PageKeeper($rootScope, $state, $cookieStore, $http){
        
        $rootScope.globals = $cookieStore.get('globals') || {};
        
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common.Authorization = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }
    
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
            
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = toState.data.requireLogin;
            var loggedIn = $rootScope.globals.currentUser;
            
            if (loggedIn && toState.name == 'login')
            {
                event.preventDefault();
                $state.go('courses.list');
            }
            
            if (restrictedPage && !loggedIn) {
                event.preventDefault();
                $state.go('login');
            }
        });
    }
})();