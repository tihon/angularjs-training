(function() {
    'use strict';
    
    angular
        .module('VideoCatalog')
        .config(Routing);

    Routing.$inject = ['$urlRouterProvider', '$stateProvider'];
        
    function Routing($urlRouterProvider, $stateProvider) {
            
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'login/login.view.html',
                controller: 'LoginController',
                controllerAs: 'loginvm',
                data: {
                    displayName: false,
                    requireLogin: false
                }
            });
        
        $stateProvider
            .state('courses', {
                url: '/courses',
                template: '<ui-view />',
                data: {
                    displayName: 'Courses',
                    requireLogin: true
                }
            })
        
            .state('courses.list', {
                url: '/list',
                templateUrl: 'courses/courses.view.html',
                controller: 'CoursesController',
                controllerAs: 'coursesvm',
                data: {
                    displayName: false,
                    requireLogin: true
                }
            })
            
            .state('courses.add', {
                url: '/add',
                templateUrl: 'courses/course/course.view.html',
                controller: 'CourseController',
                controllerAs: 'coursevm',
                data: {
                    requireLogin: true,
                    displayName: 'Add new course'
                }
            })
            
            .state('courses.edit', {
                url: '/:courseId',
                templateUrl: 'courses/course/course.view.html',
                controller: 'CourseController',
                controllerAs: 'coursevm',
                resolve: {
                        course: function(CoursesLocalStorageFactory, $stateParams){
                            return CoursesLocalStorageFactory.get($stateParams.courseId);
                        }
                },
                data: {
                    displayName: '{{course.name}}',
                    requireLogin: true
                }
            });
            
        $urlRouterProvider.otherwise('/login');
    }
})();