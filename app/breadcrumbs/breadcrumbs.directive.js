(function() {
    'use strict';

    angular
        .module('VideoCatalog.breadcrumbs')
        .directive('vcdBreadcrumbs', Breadcrumbs); 
            
    Breadcrumbs.$inject = ['$state', '$interpolate'];
    
    function Breadcrumbs($state, $interpolate) {            
        return {
            restrict: 'E',
            templateUrl: 'breadcrumbs/breadcrumbs.view.html',
            controller: 'BreadcrumbsController',
            controllerAs: "breadcrumbsvm",
            scope: {
                displaynameProperty: '@',
                updateCrumb: '&'
            }
        };
    }
})();