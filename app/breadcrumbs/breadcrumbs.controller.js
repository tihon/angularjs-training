(function() {
    'use strict';

    angular
        .module('VideoCatalog.breadcrumbs')
        .controller('BreadcrumbsController', BreadcrumbsController);
     
    BreadcrumbsController.$inject = ['$scope', '$state', '$interpolate'];
    
    function BreadcrumbsController($scope, $state, $interpolate)
    {
        $scope.breadcrumbs = [];
        
        if ($state.$current.name !== '') {
            updateBreadcrumbsArray();
        }
        
        $scope.$on('$stateChangeSuccess', function() {
            updateBreadcrumbsArray();
        });

        function updateBreadcrumbsArray() {
            var workingState;
            var displayName;
            var breadcrumbs = [];
            var currentState = $state.$current;

            while(currentState.parent) {
                
                workingState = currentState;
                
                if (workingState) {
                    displayName = getDisplayName(workingState);

                    if (displayName !== false && !stateAlreadyInBreadcrumbs(workingState, breadcrumbs)) {
                        breadcrumbs.unshift({ displayName: displayName, route: getRoute(workingState) });
                    }
                }
                
                currentState = currentState.parent;
            }
            
            $scope.breadcrumbs = breadcrumbs;
        }

        // workaround
        function getRoute(workingState)
        {
            if (workingState.name == 'courses')
            {
                return 'courses.list';
            }
            
            return workingState.name;
        }

        function getDisplayName(currentState) {
            var interpolationContext;
            var propertyReference;
            var displayName;

            if (!$scope.displaynameProperty) {
                return currentState.name;
            }
            
            propertyReference = getObjectValue($scope.displaynameProperty, currentState);

            if (propertyReference === false) {
                return false;
            } else if (typeof propertyReference === 'undefined') {
                return currentState.name;
            } else {
                interpolationContext =  (typeof currentState.locals !== 'undefined') ? currentState.locals.globals : currentState;
                displayName = $interpolate(propertyReference)(interpolationContext);
                return displayName;
            }
        }

        function getObjectValue(objectPath, context) {
            var i;
            var propertyArray = objectPath.split('.');
            var propertyReference = context;

            for (i = 0; i < propertyArray.length; i ++) {
                if (angular.isDefined(propertyReference[propertyArray[i]])) {
                    propertyReference = propertyReference[propertyArray[i]];
                } else {
                    return undefined;
                }
            }
            
            return propertyReference;
        }

        function stateAlreadyInBreadcrumbs(state, breadcrumbs) {
            var i;
            var alreadyUsed = false;
            
            for(i = 0; i < breadcrumbs.length; i++) {
                if (breadcrumbs[i].route === state.name) {
                    alreadyUsed = true;
                }
            }
            
            return alreadyUsed;
        }
    }
})(); 