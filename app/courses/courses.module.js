(function() {
    'use strict';
    
    angular.module('VideoCatalog.courses', ['ui.bootstrap', 
                                            'VideoCatalog.courses.modal']);
                                 
})();