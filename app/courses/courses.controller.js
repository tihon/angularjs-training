(function() {
    'use strict';

	angular
		.module('VideoCatalog.courses')
		.controller('CoursesController', CoursesController);
	
	CoursesController.$inject = ['$filter', '$state', 'ModalService', 'CoursesResourseFactory', 'CoursesModelService', 'CoursesFilterService', 'HeaderInfoService'];

	function CoursesController($filter, $state, ModalService, CoursesResourseFactory, CoursesModelService, CoursesFilterService, HeaderInfoService) {

		var coursesvm = this;
		
		coursesvm.remove = remove;
		coursesvm.filter = filter;

		coursesvm.model = CoursesModelService.model;
		
		restoreCourses();
		
		function remove(course) {
			removeCourseWithConfirmation(course);
		}
	
		function filter() {
			restoreCourses(CoursesFilterService.filter);
		}
		
		// private functions
		
		function removeCourseWithConfirmation(course) {
			ModalService.show(course, 'course.deleteconfirmation.modal.view.html').result.then(
				function (data) { removeCourse(data); }
			);	
		}
		
		function removeCourse(course)
		{
			CoursesResourseFactory.remove({courseId:course.id});
			removeCourseFromUi(course);
		}
		
		function removeCourseFromUi(course)
		{
			var index = coursesvm.model.courses.indexOf(course);
			coursesvm.model.courses.splice(index, 1);
			HeaderInfoService.courses.amount--;
		}
		
		function restoreCourses(callback) {
			CoursesResourseFactory.query(function(result) {
				coursesvm.model.courses = cleanResponse(result);
				HeaderInfoService.courses.amount = coursesvm.model.courses.length;
				if (callback) {
					callback(coursesvm.searchString);
				}	
			});
		}
		
		// workaround
		function cleanResponse(resp) {
			return JSON.parse(angular.toJson(resp));
		}
	}
})();