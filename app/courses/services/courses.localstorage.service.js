(function() {
    'use strict';
	
	angular
		.module('VideoCatalog.courses')
		.factory('CoursesLocalStorageFactory', CoursesLocalStorageFactory);
		
	CoursesLocalStorageFactory.$inject = ['$resource', 'localStorageService', '$filter'];
		
	function CoursesLocalStorageFactory($resource, localStorageService, $filter)
	{
		var service = {};
		
		service.query = query;
		service.get = get;
		service.add = add;
		service.update = update;
		service.remove = remove;
		
		service.restoreLocalStorage = restoreLocalStorage;
		
		return service;
		
		function query() {
			var keys = localStorageService.keys();
			var courseIds = $filter('filter')(keys, "course_");
			
			var courses = [];
			
			for(var i = 0; i< courseIds.length; i++)
			{
				courses.push(localStorageService.get(courseIds[i]));
			}
			
			return courses;
		}
	
		function get(id) {
			return localStorageService.get(getCourseIdByCourseId(id));
		}
		
		function add(course) {
			course.id = getNextId();
			save(course);
		}
	
		function update(course) {
			remove(course.id);
			save(course);
		}
	
		function remove(id) {
			localStorageService.remove(getCourseIdByCourseId(id));
		}	
	
		function restoreLocalStorage(courses) {
				angular.forEach(courses, function (course) {
					save(course);
				});
		}
		
		// private functions
		
		function save(course) {
			localStorageService.set(getCourseIdByCourseId(course.id), course);
		}

		function getCourseIdByCourseId(id) {
			return "course_" + id;
		}
		
		function getNextId() {
			
			var courses = query();
			var coursesIds = [];
			
			for(var i = 0; i < courses.length; i++)
			{
				coursesIds.push(courses[i].id);
			}
			
			var maxIds = Math.max.apply(null, coursesIds);
			
			return ++maxIds;
		}
	}
})();