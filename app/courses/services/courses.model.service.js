(function() {
    'use strict';

	angular
		.module('VideoCatalog.courses')
		.service('CoursesModelService', [CoursesModelService]);

	CoursesModelService.$inject = ['$filter'];
	
	function CoursesModelService($filter) {
		
		var service = this;
		service.resetFiltered = resetFiltered;
		
		service.model = {
			courses : [],
			filtered : []
		};
		
		return service;
		
		function resetFiltered(){
			service.model.filtered = [];
		}
	}
})();