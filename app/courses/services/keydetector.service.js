(function() {
    'use strict';
    
    angular
        .module('VideoCatalog.courses')
        .service('KeyDetector', KeyDetector);
        
    function KeyDetector() {
        
        var service = this;
        
        service.isNumericKeyCode = isNumericKeyCode;
        service.isForwardDotKeyCode = isForwardDotKeyCode;
        service.isNavigationKeyCode = isNavigationKeyCode;
        service.isCtrlPlusV = isCtrlPlusV; 
                
        return service;
        
        function isNumericKeyCode(keyCode) {
            return (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105);
        }
        
        function isForwardDotKeyCode(keyCode) {
            return event.keyCode == 190;
        }
        
        function isNavigationKeyCode(keyCode) {
            
            switch (keyCode)
            {
                case 8: // backspace
                case 35: // end
                case 36: // home
                case 37: // left
                case 38: // up
                case 39: // right
                case 40: // down
                case 45: // ins
                case 46: // del
                    return true;
                default:
                    return false;
            }
        }		
        
        function isCtrlPlusV(event) {
            return (event.ctrlKey && (event.which == 86 || event.which==118));
        }
    }
})();