(function() {
    'use strict';

	angular
		.module('VideoCatalog.courses')
		.service('CoursesFilterService', CoursesFilterService);
		
		CoursesFilterService.$inject = ['$filter', 'CoursesModelService'];
		
		function CoursesFilterService($filter, CoursesModelService) {
			
			var service = this;
			service.filter = filter;
						
			return service;
			
			function filter(searchString){
				CoursesModelService.resetFiltered();
				
				if (isStartDate(searchString)) {
					CoursesModelService.model.filtered = filterByStartDate(searchString);
				}
				else if (isDuration(searchString)) {
					var duration = String(convertTexttoDuration(searchString));
					CoursesModelService.model.filtered = $filter('filter')(CoursesModelService.model.courses, { duration: duration } );
				}
				else {
					CoursesModelService.model.filtered = $filter('filter')(CoursesModelService.model.courses, { name: searchString });
				}
				
				if (searchString)
				{
					CoursesModelService.model.courses = CoursesModelService.model.filtered;
				}
			}
			
			function filterByStartDate(date)
			{
				var dateparts = date.split("."); 
				
				var searchDate = new Date(parseInt(dateparts[2]), parseInt(dateparts[1]) - 1, parseInt(dateparts[0]));
				var filtered = [];
				
				angular.forEach(CoursesModelService.model.courses, function(item, key){
					var courseDate = new Date(parseInt(item.startDate));
		
					if (courseDate.setHours(0,0,0,0) == searchDate.setHours(0,0,0,0))
					{
						filtered.push(item);
					}
				});
				
				return filtered;
			}
			
			function isDuration(text)
			{
				return /^([\d]{1,2}\s+Hour[s]?\s*)?([\d]{1,2}\s+min)?$/i.test(text);
			}
			
			function convertTexttoDuration(text)
			{
				var hours = 0;
				var minutes = 0;
				
				var parts = text.trim().replace(/\s+/, " ").split(" ");
				
				switch(parts.length)
				{
					case 4:
						hours = parseInt(parts[0]);
						minutes = parseInt(parts[2]);
						break;
					case 2:
						if (parts[1].toUpperCase() == "HOUR" || parts[1].toUpperCase() == "HOURS"){
							hours = parseInt(parts[0]);
						} else if(parts[1].toUpperCase() == "MIN") {
							minutes = parseInt(parts[0]);
						}
						break;
				}
				var duration = hours * 60 + minutes;
				return duration;
			}
			
			function isStartDate(text)
			{
				return /^[\d]{1,2}\.[\d]{1,2}\.[\d]{4}$/i.test(text);
			}			
					
		}
})();