(function() {
    'use strict';

	angular
		.module('VideoCatalog.courses')
		.factory('CoursesResourseFactory', CoursesResourseFactory);
		
	CoursesResourseFactory.$inject = ['$resource'];
	
	function CoursesResourseFactory($resource)
	{
		return $resource('courses/:courseId', {}, {
			query: { method:'GET', params:{ courseId:'list' }, isArray:true },
			remove: { method:'DELETE', isArray:false }
		});
	}
})();