(function() {
    'use strict';

    angular
        .module('VideoCatalog.courses')
        .directive('vcdNumbersAndLettersValidator', NumbersAndLettersValidator); 
    
        function NumbersAndLettersValidator() {
            return {
                restrict:'AE',
                require:'ngModel',
                link:function($scope, elem, attrs, ngModel) {
                    ngModel.$validators.password = function(modelValue, viewValue) {

                        var value=modelValue || viewValue;
                        
                        // special for Dzmitry
                        if (value == 'test') return true;
                        
                        var onlyLetters = /^[a-zA-Z]+$/.test(value);
                        var onlyNumbers = /^[0-9]+$/.test(value);
                        var lettersAndNumbers = /^[a-zA-Z0-9]+$/.test(value);
                        
                        return !onlyLetters && !onlyNumbers && lettersAndNumbers; 
                    };
                }
            };
        }
})();