(function() {
    'use strict';
	
	angular
		.module('VideoCatalog.courses')
		.directive('vcdDateKeys', DateKeys);
		
	DateKeys.$inject = ['KeyDetector'];
	
	function DateKeys(KeyDetector) {
		return {
			restrict : 'A',
			link: function(scope, element, attrs, controller) {
						element.on('keydown', function(event) {

						if (KeyDetector.isNumericKeyCode(event.keyCode) || KeyDetector.isForwardDotKeyCode(event.keyCode) || KeyDetector.isNavigationKeyCode(event.keyCode)) {
							
							var inputLikeDate = inputSimilarToDate(element.val() + getChar(event.keyCode));
							var navigationKeys = KeyDetector.isNavigationKeyCode(event.keyCode);
							
							if (inputLikeDate && !navigationKeys) { 
								return true;
							}
							
							event.preventDefault();
							return false;
						}
						
						event.preventDefault();
						return false;
				});
			}
		};
		
		// private methods
		
		function inputSimilarToDate(input)
		{
			return /^([\d]{1,2})?([\.])?([\d]{1,2})?([\.]?)([\d]{1,4})?$/.test(input);
		}
		
		function getChar(code)
		{
			var input = String.fromCharCode(code);
			
			if (code == 190 || code == 110)
			{
				input = '.';
			}
			
			return input;
		}
	}
})();