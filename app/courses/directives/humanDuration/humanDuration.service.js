(function() {
    'use strict';
    angular
        .module('VideoCatalog.courses')
        .service('HumanDurationService', HumanDurationService);
        
    function HumanDurationService() {
        
        var service = this;
        
        service.getHumanReadableDuration = getHumanReadableDuration;
                
        return service;
        
        function getHumanReadableDuration(duration)
        {
            var resultText = "";
            
            var hours = duration / 60 | 0;
            var minutes = duration % 60;
            
            if (hours > 0) {
                resultText = hours + ' hour';
            }
            
            if (hours > 1) {
                resultText += 's';
            }
            
            if (minutes > 0) {
                resultText += ' ' + minutes + ' min';
            }
            
            return resultText;
        }
    }
})();