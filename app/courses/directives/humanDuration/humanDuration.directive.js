(function() {
    'use strict';
    
    angular
        .module('VideoCatalog.courses')
        .directive('vcdHumanDuration', HumanDuration);
    
    HumanDuration.$inject = ['$compile', 'HumanDurationService'];
    
    function HumanDuration($compile, HumanDurationService)
    {
        return {
            restrict : 'EA',
            require: '?ngModel',
            replace: true,
            template: '<span>{{modelValue()}}</span>',
            link: function(scope, element, attrs, ngModel) {
                scope.modelValue = function () { 
                    return HumanDurationService.getHumanReadableDuration(ngModel.$viewValue); 
                };
            }
        };
    }
})();