/* not used !!! */
(function() {
    'use strict';
    
    angular
        .module('VideoCatalog.courses')
        .directive('vcdTimeDuration', TimeDuration);
    
    TimeDuration.$inject = ['$compile', 'HumanDurationService'];
    
    function TimeDuration($compile, HumanDurationService)
    {
        return {
            restrict : 'A',
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                initPlacehiolder(scope, element, attrs);
                scope.$watch( 
                        function() { return ngModel.$modelValue;}, 
                        function (newVal, oldVal) { scope.newVal = HumanDurationService.getHumanReadableDuration(ngModel.$modelValue); }
                );
            }
        };
        
        // private functions
        
        function initPlacehiolder(scope, element, attrs) {
            
            var id = attrs.timeDuration;
            
            if (!id) {
                return;
            }
            
            var targetElem = angular.element(document.querySelector('#' + id));
            var template = '<span class="help-block">{{newVal}}</span>';
            var content = $compile(template)(scope);
            targetElem.append(content);
        }
    }
})();