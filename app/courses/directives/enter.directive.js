(function() {
    'use strict';
    
    angular
        .module('VideoCatalog.courses')
        .directive('vcdEnter', Enter);
        
        function Enter() {
            return function(scope, element, attrs) {
                element.bind("keydown keypress", function(event) {
                    if(event.which === 13) {
                        scope.$apply(function(){
                            scope.$eval(attrs.vcdEnter, {'event': event});
                        });
    
                        event.preventDefault();
                    }
                });
            };
        }
})();