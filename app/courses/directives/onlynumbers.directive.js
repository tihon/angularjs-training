(function() {
    'use strict';
	
	angular.module('VideoCatalog.courses')
	.directive('vcdOnlyNumbers', OnlyNumbers);
	
	OnlyNumbers.$inject = ['KeyDetector'];
	
	function OnlyNumbers(KeyDetector)
	{
		return {
			restrict : 'A',
			link: function(scope, element, attrs, controller) {
					element.on('keydown', function(event) {
						if (KeyDetector.isNumericKeyCode(event.keyCode) || KeyDetector.isForwardDotKeyCode(event.keyCode) || KeyDetector.isNavigationKeyCode(event.keyCode) || KeyDetector.isCtrlPlusV(event)) {
					
							return true;
						}
						
						event.preventDefault();
						return false;
					});
			}
		};
	}
})();