(function() {
    'use strict';
	
	angular
		.module('VideoCatalog.courses')
		.directive('vcdFixDatetimepickerFormat', FixDateTimePickerFormat);
		
	function FixDateTimePickerFormat() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModel) {
					ngModel.$parsers.push(function(viewValue) { 
						return +viewValue; 
					});
			}
		};
	}
})();