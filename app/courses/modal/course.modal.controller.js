(function() {
    'use strict';
    angular
        .module('VideoCatalog.courses.modal')
        .controller('ModalController', ModalController);
    
    ModalController.$inject = ['$modalInstance', 'data'];
    
    function ModalController($modalInstance, data) {
        
        var modalvm = this;
        
        modalvm.ok = ok;
        modalvm.no = no;
        
        modalvm.data = data;
        
        function ok() {
            $modalInstance.dismiss("ok");
        }
        
        function no() {
            $modalInstance.close(modalvm.data);
        }
    }
})();