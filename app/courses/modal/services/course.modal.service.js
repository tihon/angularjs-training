(function() {
    'use strict';

    angular
        .module('VideoCatalog.courses.modal')
        .service('ModalService', ModalService);
            
    ModalService.$inject = ['$modal'];
    
    function ModalService($modal) {
        
        var service = {};
    
        service.show = show;
        service.data = {};
        
        return service;
        
        function show(data, template)  {
            
            service.data = data;
            
            return $modal.open({
                templateUrl: 'courses/modal/' + template,
                controller: 'ModalController',
                controllerAs: 'modalvm',
                backdrop: 'static', 
                keyboard: false,
                resolve: {
                    data: function () { return service.data; }
                }
            });
        }
    }
})();