(function() {
    'use strict';

	angular
		.module('VideoCatalog.courses')
		.controller('CourseController', CourseController);
	
	CourseController.$inject = ['$state', '$filter', '$stateParams', 'CoursesResourseFactory', 'CourseModelService', 'AuthorsResourceService', 'ModalService', 'CourseValidationFactory', 'HeaderInfoService'];
	
	function CourseController($state, $filter, $stateParams, CoursesResourseFactory, CourseModelService, AuthorsResourceService, ModalService, CourseValidationFactory, HeaderInfoService) {
		
		var coursevm = this;
	
		coursevm.cancel = cancel;
		coursevm.submit = submit;
		coursevm.removeAuthors = removeAuthors;
		coursevm.addAuthors = addAuthors;
		coursevm.openDatetimePicker = openDatetimePicker;
		
		coursevm.targetAuthors = [];
		coursevm.sourceAuthors = [];
		coursevm.errors = [];
		
		init();
	
		function cancel() {
			if (coursevm.form.$dirty)	{
				ModalService.show({}, 'course.cancelediting.modal.view.html').result.then(
					function(response) { $state.go('courses.list'); }
				);
			}
			else {
				$state.go('courses.list');
			}
		}
		
		function submit() {
			
			coursevm.errors = CourseValidationFactory.validate(coursevm.courseModel, CourseModelService.mode.isEdit);
			
			if (coursevm.errors.length > 0) {
				ModalService.show(coursevm.errors, 'course.validation.modal.view.html');
			} else {
				if (CourseModelService.mode.isEdit) {
					CoursesResourseFactory.save({courseId:coursevm.courseModel.id}, coursevm.courseModel);
				} else {
					CoursesResourseFactory.save(coursevm.courseModel);
					HeaderInfoService.courses.amount++;	
				}
				
				$state.go('courses.list'); 
			}
		}
			
		function openDatetimePicker($event) {
			coursevm.datetimepicker.status.opened = true;
		}			

		 function addAuthors() {
			angular.forEach(coursevm.selectedSourceAuthors, function(value, key) {
				coursevm.targetAuthors.push(value);
				coursevm.sourceAuthors = removeElement(value, coursevm.sourceAuthors);
			});
			
			syncAuthors();
		}
		
		function removeAuthors() {
			angular.forEach(coursevm.selectedTargetAuthors, function(value, key) {
				coursevm.sourceAuthors.push(value);
				coursevm.targetAuthors = removeElement(value, coursevm.targetAuthors);
			});
			
			syncAuthors(); 
		}
		
		// private functions
		
		function init()	{
			
			coursevm.courseModel = CourseModelService.model;
			
			setMode();
					
			if (CourseModelService.mode.isEdit) {
				initEditMode();				
			} else {
				initAddMode();
			}
			
			initDateTimePicker();
			initAuthors();
		}
		
		function initAuthors() {
			var authors = AuthorsResourceService.query();
		
			authors.$promise.then(function(data) {
				coursevm.sourceAuthors = data;
				
				if (CourseModelService.mode.isEdit) {
					
					angular.forEach(coursevm.courseModel.authors, function(value, key) {
						if (coursevm.sourceAuthors.indexOf(value)) {
							coursevm.sourceAuthors = removeElement(value, coursevm.sourceAuthors);
						}
					});
				}
			});
		}
		
		function initDateTimePicker() {
			coursevm.datetimepicker = {
					format : 'dd.MM.yyyy',
					status : { opened: false },
					minDate :new Date().setHours(0,0,0,0)
			};
		}
		
		function initAddMode() {
			coursevm.courseModel.startDate = new Date().setHours(0,0,0,0);
		}
		
		function initEditMode() {
				CoursesResourseFactory.get({courseId : $stateParams.courseId}, function(data){
					coursevm.courseModel = data;
					coursevm.targetAuthors = coursevm.courseModel.authors || [];
				});
		}
		
		function syncAuthors() {
			coursevm.courseModel.authors = coursevm.targetAuthors;
		}
		
		function setMode()
		{
			CourseModelService.mode.isEdit = $stateParams.courseId !== undefined;
		}
		
		function removeElement(element, array) {
			var elements = $filter('filter')(array, { "id" : element.id });
			if (elements) { array.splice(array.indexOf(elements[0]), 1); }
			return array;
		}
	}
})();