(function() {
    'use strict';
	angular
		.module('VideoCatalog.courses')
		.service('CourseModelService', CourseModelService);
		
	function CourseModelService()
	{
		var service = this;
		
		service.model = {
			name : "",
			description: "",
			duration: "",
			startDate: "",
			authors: []
		};
		
		service.mode = {
			isEdit: false
		};
		
		return service;
	}
})();