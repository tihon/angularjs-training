(function() {
    'use strict';
	angular
		.module('VideoCatalog.courses')
		.factory('CourseValidationFactory', CourseValidationFactory);
			
	function CourseValidationFactory() {
		
		var service = {};
		
		service.validate = validate;
		service.errors = errors;
		
		var foundErrors = [];

		return service;
		
		function validate(course, isEdit) {
			
			cleanUpErrors();
			
			validateCourseName(course.name);
			validateDescription(course.description);
			validateStartDate(course.startDate, isEdit);
			validateDuration(course.duration);
			validateAuthors(course.authors);
			
			return errors();
		}
		
		function errors() {
			return foundErrors;
		}
		
		// private functions
		
		function validateStartDate(startDate, isEdit) {
			
			if (isEmpty(startDate))
			{
				addError("StartDate", "Start date cannot be empty");
			} else {
				if (!isStartDateActual(startDate, isEdit)){
					addError("StatDate", "Start date canont be in past");
				}
			}
		}
	
		function validateDuration(duration) {
			
			if (isEmpty(duration))
			{
				addError("Duration", "Duration cannot be empty");
			} else {
				if (duration < 10 || duration > 480){
					addError("Duration", "Duration cannot be less then 10 minutes and longer then 8 hours.");
				}
			}
		}
	
		function validateAuthors(authors) {
				if (authors === undefined || authors.length === 0)
				{
					addError("Authors", "Author was not added.");
				}
		}
	
		function isStartDateActual(startDate, isEdit) {
			return isEdit ? true : (new Date(startDate) > new Date(Date.now()));
		}
	
		function validateDescription(description) {
			
			if (isEmpty(description)) {
				addError('description', "Description can't be empty");
			} else {
				if (tooLong(description, 1000) || tooShort(description, 20)) {
					addError('description', "Description must be between 20 and 1000 characters long.");
				}
			}
		}
		
		function validateCourseName(name) {
			
			if (isEmpty(name)){
				addError('name', "Name can't be empty");
			} else {
				if (tooLong(name, 50) || tooShort(name, 3)) {
					addError('name', "Name must be between 3 and 50 characters long.");
				}
			}
		}
		
		function tooLong(field, limit) {
			return field.length > limit;
		}
		
		function tooShort(field, limit) {
			return field.length < limit;
		}
		
		function isEmpty(field) {
			if (Array.isArray(field))
			{
				return field.length > 0;
			}
			
			return !field;
		}
		
		function addError(field, message) {
			foundErrors.push({"field" : field, "message" : message});
		}
		
		function cleanUpErrors() {
			foundErrors = [];
		}
	}
})();