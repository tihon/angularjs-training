(function() {
    'use strict';

	angular
		.module('VideoCatalog.header')
		.factory('HeaderInfoService', HeaderInfoService);
		
	function HeaderInfoService()
	{
		var service = {};
		
		service.courses = { amount: 0 };
		
		return service;
	}
})();