(function() {
    'use strict';

	angular
		.module('VideoCatalog.header')
		.controller('HeaderController', HeaderController);
		
	HeaderController.$inject = ['$state', 'AuthService', 'HeaderInfoService'];
	
	function HeaderController($state, AuthService, HeaderInfoService) {
		
		var headervm = this;
		
		headervm.LoggedUser = {};
		headervm.logout = logout;
		
		headervm.courses = HeaderInfoService.courses;
	
		checkLoggedUser();
		
		function checkLoggedUser() {
			AuthService.updateActiveUser();
			headervm.LoggedUser = AuthService.ActiveUser;
		}
		
		function logout() {
			AuthService.logout();
			
			$state.go('login');
		}
	}
})();