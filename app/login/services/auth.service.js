(function() {
    'use strict';

angular
	.module('VideoCatalog.login')
	.service('AuthService', AuthService);
		
    AuthService.$inject = ['$filter', '$http', '$cookieStore', '$rootScope', 'UserLocalStorageService', 'UserResourceService'];
        
    function AuthService($filter, $http, $cookieStore, $rootScope, UserLocalStorageService, UserResourceService) {
        
        var service = {};
    
        service.login = login;
        service.logout = logout;
        service.setCredentials = setCredentials;
        service.updateActiveUser = updateActiveUser;
        
        service.ActiveUser = { username: '' };
        
        return service;
        
        function login(username, password, callback)
        {
            UserResourceService.query(function(users){
				
				var filtered = $filter('filter')(users, { username: username });
				var user = filtered.length ? filtered[0] : null;
				
				callback(validateLogin(username, password, user));
			});
        }
        
        function logout() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            UserLocalStorageService.removeActiveUser();
            $http.defaults.headers.common.Authorization = 'Basic ';
			service.ActiveUser.username = '';
        }
        
        function setCredentials(email, password) {
            
            var authdata = Base64.encode(email + ':' + password);
    
            $rootScope.globals = {
                currentUser: {
                    username: email,
                    authdata: authdata
                }
            };
    
            $http.defaults.headers.common.Authorization = 'Basic ' + authdata;
            $cookieStore.put('globals', $rootScope.globals);
        }
        
        function updateActiveUser() {
            
            $rootScope.globals = $cookieStore.get('globals') || {};
            var loggedUser = $rootScope.globals.currentUser;
            
            if (loggedUser) {
                service.LoggedIn = true;
                service.ActiveUser.username = loggedUser.username;
                return UserLocalStorageService.getActiveUser();
            }
            
            service.ActiveUser.username = '';
            
            return null;
        }
        
        // private functions
        
        function validateLogin(username, password, user)
        {
            if (user !== null && user.password === password) {
                UserLocalStorageService.save(user);
                service.ActiveUser.username = user.username;
                return true;
            } else {
                UserLocalStorageService.remove(user);
                service.ActiveUser.username = '';
                
                return false;
            }
        }
    }
    
    // Base64 encoding service used by AuthenticationService
    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };		
})();