(function() {
    'use strict';
	
	angular
		.module('VideoCatalog.login')
		.service('UserLocalStorageService', UserLocalStorageService);
		
	UserLocalStorageService.$inject = ['$filter', 'localStorageService', 'UserResourceService'];
		
	function UserLocalStorageService($filter, localStorageService, UserResourceService) {
	
		var service = {};
		
		service.save = save;
		service.remove = remove;
		service.getActiveUser = getActiveUser;
		service.removeActiveUser = removeActiveUser;
		
		return service;
		
		function save(user) {
			user.password = "secure_password";
			localStorageService.set('activeUser', JSON.stringify(user));
		}
		
		function remove(user) {
			localStorageService.remove('activeUser');
		}
		
		function getActiveUser() {
			var user = localStorageService.get('activeUser');
			return JSON.parse(user);
		}
		
		function removeActiveUser(){
			localStorageService.remove('activeUser');
		}
			
	}
})();