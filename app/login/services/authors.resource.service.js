(function() {
    'use strict';

	angular
		.module('VideoCatalog.login')
		.factory('AuthorsResourceService', AuthorsResourceService);
		
	AuthorsResourceService.$inject = ['$resource'];
		
	function AuthorsResourceService($resource)
	{
		return $resource('app-storage/authors');
	}
})();