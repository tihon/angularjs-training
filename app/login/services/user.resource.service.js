(function() {
    'use strict';
	
	angular
		.module('VideoCatalog.login')
		.factory('UserResourceService', UserResourceService);
		
	UserResourceService.$inject = ['$resource'];
	
	function UserResourceService($resource) {
		return $resource('app-storage/users');
	}
})();