(function() {
    'use strict';
    
    angular
        .module('VideoCatalog.header')
        .controller('LoginController', LoginController);
    
    LoginController.$inject = ['$rootScope', '$state', '$location', 'AuthService', 'HeaderInfoService'];
            
    function LoginController ($rootScope, $state, $location, AuthService, HeaderInfoService) {

        var loginvm = this;
        
        loginvm.login = login;
        loginvm.pattern = /^[a-zA-Z]+$/;
        loginvm.error = false;
        
        HeaderInfoService.courses.amount = 0;
        
        function login() {
            AuthService.login(loginvm.username, loginvm.password, function(ok) {
                if (ok) {
                    AuthService.setCredentials(loginvm.username, loginvm.password);
                    AuthService.LoggedIn = true;
                    $state.go('courses.list');
                } else {
                    loginvm.error = "Wrong login or password";
                    AuthService.LoggedIn = false;
                    loginvm.form.$setPristine();
                    loginvm.password = "";
                }
            });
        }
    }
})();