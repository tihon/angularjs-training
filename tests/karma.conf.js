module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
       'bower_components/angular/angular.js',
       'bower_components/angular-route/angular-route.js',
       'bower_components/angular-mocks/angular-mocks.js',

       'app/breadcrumbs/breadcrumbs.module.js',
       'app/header/header.module.js',
       'app/header/header.controller.js',
 
       'tests/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    },

    port: 8000,
    urlRoot: '/'

  });
};
