(function(){
	'use strict'	
	
	describe("VideoCatalog.header module", function() {
		
		var controller;

		var stateMock = {
			toState : {},
			go: function(state) { this.toState = state; }
		}
		
		var authServiceMock = {
			ActiveUser: { username: '' },
			logout: function() {},
			updateActiveUser: function() { this.ActiveUser.username = "ser"; }
		}
		
		var headerInfoServiceMock = {
		}

		beforeEach(function() {
			
			module('VideoCatalog.header');
		
			inject(function($controller) {
				controller = $controller;
			});
		});
		  
		describe('HeaderController tests', function() {
			
			it('1. Shoud call function updateActiveUser ', function() {
				
				spyOn(authServiceMock, 'updateActiveUser');
				
				var ctrl = controller('HeaderController', { $state : stateMock, AuthService : authServiceMock, HeaderInfoService: headerInfoServiceMock });
				
				expect(authServiceMock.updateActiveUser).toHaveBeenCalled();
			})

			it('2. Shoud set property \'LoggedUser\'', function() {
				
				spyOn(authServiceMock, 'updateActiveUser');
				
				var ctrl = controller('HeaderController', { $state : stateMock, AuthService : authServiceMock, HeaderInfoService: headerInfoServiceMock });
				
				expect(ctrl.LoggedUser).toBeDefined();
			})

			it('3. Shoud call function \'logout\'', function() {
				
				spyOn(authServiceMock, 'logout');
				
				var ctrl = controller('HeaderController', { $state : stateMock, AuthService : authServiceMock, HeaderInfoService: headerInfoServiceMock });
				ctrl.logout();
				
				expect(authServiceMock.logout).toHaveBeenCalled();
			})
			
			it('4. Shoud call function \'go\' of \'state\' service', function() {
				
				spyOn(stateMock, 'go');
				
				var ctrl = controller('HeaderController', { $state : stateMock, AuthService : authServiceMock, HeaderInfoService: headerInfoServiceMock });
				ctrl.logout();
				
				expect(stateMock.go).toHaveBeenCalled();
			})
			
			it('5. Shoud go to state \'login\'', function() {
				
				spyOn(stateMock, 'toState');
				
				var ctrl = controller('HeaderController', { $state : stateMock, AuthService : authServiceMock, HeaderInfoService: headerInfoServiceMock });
				
				ctrl.logout();
				
				expect(stateMock.toState).toEqual('login');
			})
		})
	});
})();