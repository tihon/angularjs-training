var gulp = require('gulp'); 
var jshint = require('gulp-jshint');
var wiredep = require('wiredep').stream;
var inject = require('gulp-inject');
var angularFilesort = require('gulp-angular-filesort');
 
gulp.task('index', function() {
    gulp.src('./app/index.html')
    .pipe(inject(
        gulp.src(['./app/**/*.js']).pipe(angularFilesort())
    ))
    .pipe(gulp.dest('./app'));
});

gulp.task('jshint', function() {
  gulp.src('./app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('bower', function () {
  gulp.src('./app/index.html')
    .pipe(wiredep({
      optional: 'configuration',
      goes: 'here'
    }))
    .pipe(gulp.dest('./app'));
});

gulp.task('default', function() {
  
  var src = ['jshint', 'index'];
  var bower = ['bower'];
  
  gulp.watch('./app/**/*.js', src);
  gulp.watch('./bower_components/**', bower);
  
});

function swallowError(error) {
    console.log(error.toString());
}